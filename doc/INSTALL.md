### Building snoopski from sources

_____the snoopski gitlab project repository contains a source tree
_____which is (hopefully) buildable via dpkg-buildpackage
_____(or pbuilder, or other similar debian packaging utility)
_____
_____THESE CONFIGURATION / INSTALLATION DETAILS, BELOW, ARE SOMEWHAT MOOT
_____YET THEY ARE ARGUABLY WORTH READING


___________________


### Autoscan - running manually ( manually, i.e. not using a debian packaging utility)

AS1: Clean out the project source tree, or else autoscan picks up unrelated dependencies
from build/aux/ltmain.sh, namely AC_PROG_CPP, AC_PROG_CXX and AC_PROG_RANLIB.
The bootstrap+configure steps are present here to bring the source tree into a known-good state:
```shell
./bootstrap.sh &&
./configure --enable-everything &&
make wedonothaveatarget
```

AS2: Run `autoscan` now:
```shell
autoscan
```

AS3: a `configure.scan` file is already present within the source tree, from the previous `autoscan` run.
After tweaking your configuration, you might want to recheck, regenerate a fresh configure.scan



## Autoreconf

The `autoreconf` tool (re)generates the `config.h.in` file from the latest
`configure.ac` content (potentially updated by the [Autoscan](#autoscan) step above).

Once the above [Autoscan](#autoscan)-related additions are done, update the `config.h.in` file with:
```shell
./bootstrap.sh
```


## Valgrind

Valgrind is a local diagnostic tool that checks the binary for any memory and/or file descriptor leaks.
If valgrind is installed, you may wish to run the valgrind check, as follows:
```shell
./configure --enable-everything &&
make valgrind
```
____________



#### 3.3 Build configuration

# ./bootstrap.sh
generates the ./configure script

build-depends:  autoconf, automake, libtool, m4, make, procps

snoopski supports various features that  can be enabled by supplying arguments to configure command.
Consult `./configure --help' for more information.

    # Only if you are building directly from giiit repository:
    ./bootstrap.sh

    # Check configuration options, see section 3.3 for details:
    ./configure --help

    # Then continue with normal build procedure:
    ./configure [OPTIONS]
    make
    make install

    # At this point, snoopski is **installed but not yet enabled**.
    # Enable it
    make enable

    # Reboot your system for snoopski to be picked by all programs.
    reboot


##### 3.3.1 Configuring log output

snoopski already has a default log message format configured, but by using
"./configure --with-message-format=FORMAT" you can adjust it to your needs.
(OR YOU CAN, POST-INSTALL, JUST EDIT /etc/snoopski.ini TO CUSTOMIZE THE MESSAGE FORMAT)

Log message format specification example:

    --with-message-format="text1:%{datasource1} text2:%{datasource2} text3:%{datasource3:arg}"

Text outside %{...} is considered literal and is copied as-is to final
log message. On the other hand, text found within %{...} has special
meaning: it retrieves data from specified data source. If data source
specification contains a colon, then text before colon is considered
data source name, and text following the colon is passed as an argument
to the data source provider in question.



##### 3.3.2 Configuring filtering

    # HOW TO DEFINE FILTER CHAINS
    --with-filter-chain="FILTER_CHAIN_SPEC"

By default, if FILTER_CHAIN_SPEC is not configured. An empty string is used instead,
which effectively disables filtering and all snoopski messages are passed to the configured output.
(NOTE: it is enabled, per the current debian/rules included in this source package)

See sample configuration file etc/snoopski.ini for list and description of supported filter configurations.
(CURRENTLY, ALL LINES WITHIN THE INSTALLED FILE ARE OUTCOMMENTED.
READ IT TO LEARN AVAILABLE FEATURES; ADD OR UNCOMMENT LINE FOR THE FEATURES YOU WISH TO ACTIVATE.)


##### 3.3.3 Optional configuration file support
snoopski supports optional configuration file, which may help with development and/or configuration endeavors.
Configuration file support must be enabled at build time:
(and it is enabled, per the current debian/rules included in this source package)

    --enable-config-file

Configuration file is installed as SYSCONFDIR/snoopski.ini. SYSCONFDIR
can be changed with --sysconfdir=PATH configuration directive.
See sample configuration file etc/snoopski.ini for list and description
of supported configuration directives.





### 4 How to enable/activate snoopski

#### 4.1 Enable for specific program

If you wish to monitor only certain applications you can do so through
the LD_PRELOAD environmental variable - simply set it to the full path
to libsnoopski.so shared library before starting the application.

Example:

    export LD_PRELOAD=/usr/local/lib/libsnoopski.so    # default path
    lynx http://linux.com/
    unset LD_PRELOAD



#### 4.2 Enable system-wide snoopski on 32-bit-only or 64-bit-only systems

WARNING: Using this method on multilib systems (64-bit systems capable
WARNING: of running 32-bit applications) can cause malfunction because
WARNING: preload config file /etc/ld.so.preload makes  no  distinction
WARNING: between 32- and 64-bit programs and shared libraries.

    # Use the provided snoopski-enabling script
    snoopski-enable

    # Or enable it using build tools
    make enable

Explanation:

An entry is created in /etc/ld.so.preload file  which  causes  execv()
and execve() system calls to be intercepted by snoopski and logged via syslog.



#### 4.3 For multilib systems

Content of /etc/ld.so.preload should include the following line:

    /usr/local/$LIB/libsnoopski.so

This applies only when you have installed both 32bit and 64bit version
of the library in the appropriate paths.



#### 4.4 For multilib systems with LD_PRELOAD_* environmental variables

On systems that support LD_PRELOAD_32 and LD_PRELOAD_64  you  can  use
those variables to force loading of snoopski. If you wish to  enable  it
system-wide, ensure that correct values are held  by  those  variables
at boot time. Consult section  4.a  of  this  README  on  how  to  set
environmental variables. Setting them at boot time is usually  distro -dependent.
Users are also required to compile 32-bit version of library. To do so
on 64-bit systems it is usually enough to set appropriate CFLAGS:

    CFLAGS=-m32 ./configure [OPTIONS]

Of course your system must be cross-compilation capable.
Consult  your OS documentation for details on this subject.



### 5 snoopski output

The exact location  of  your  snoopski output  depends  on  your  syslog configuration.
Usually it gets stored in one of the following files:
(NOTE: within /etc/snoopski.ini you can, instead, specify an alternate logfile)

    /var/log/auth*
    /var/log/messages
    /var/log/secure



### 6 How to disable snoopski

The simplest way is by using the provided script:

    snoopski-disable

To manually disable snoopski, simply edit /etc/ld.so.preload and remove
reference to libsnoopski.so. Also unset any environmental variable that
references snoopski (LD_PRELOAD, LD_PRELOAD_32 and LD_PRELOAD_64).
Then you may also delete snoopski shared library from  your  system.
Default installation path of snoopski shared library is:

    /usr/local/lib/libsnoopski.so
