/* File: inputdatastorage.h
 *
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __SNOOPSKI_INPUTDATASTORAGE_H
#define __SNOOPSKI_INPUTDATASTORAGE_H


//  Store execution data for inputs to consume
typedef struct {
    int          initialized;
    const char  *filename;
    char *const *argv;
    char *const *envp;
} snoopski_inputdatastorage_t;


//  Init functions
void snoopski_inputdatastorage_ctor             ();
void snoopski_inputdatastorage_dtor             ();
void snoopski_inputdatastorage_setUninitialized (snoopski_inputdatastorage_t *IDS);
void snoopski_inputdatastorage_setDefaults      (snoopski_inputdatastorage_t *IDS);

//  Functions that do the actual data storing
void snoopski_inputdatastorage_store_filename (const char *filename);
void snoopski_inputdatastorage_store_argv     (char *const argv[]);
void snoopski_inputdatastorage_store_envp     (char *const envp[]);

//  Retrieval functions
snoopski_inputdatastorage_t*   snoopski_inputdatastorage_get ();


#endif
