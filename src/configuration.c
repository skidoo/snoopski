/* File: configuration.c
 *
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Includes order: from local to global
 */
#include "configuration.h"

#include "snoopski.h"
#ifdef SNOOPSKI_CONFIGFILE_ENABLED
#include "configfile.h"
#endif
#ifdef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
#include "tsrm.h"
#endif

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

/*
 * Flag that enables/disables configuration file parsing
 *
 * This is a runtime flag, used by binaries in tests/bin/ directory.
 * ./configure flags are wrapped around this.
 */
int snoopski_configuration_configFileParsingEnabled = SNOOPSKI_TRUE;


/*
 * Should alternate configuration file be loaded?
 */
char * snoopski_configuration_altConfigFilePath = NULL;
char   snoopski_configuration_altConfigFilePathBuf[PATH_MAX] = "";


/*
 * Storage of snoopski configuration for non-thread-safe builds
 */
#ifndef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
snoopski_configuration_t   snoopski_configuration_data = {
    .initialized = SNOOPSKI_FALSE,
};
#endif


/*
 * snoopski_configuration_preinit_disableConfigFileParsing
 *
 * Description:
 *     Disables configuration file parsing at runtime.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_configuration_preinit_disableConfigFileParsing() {
    snoopski_configuration_configFileParsingEnabled = SNOOPSKI_FALSE;
}


/*
 * snoopski_configuration_preinit_enableConfigFileParsing
 *
 * Description:
 *     Enables configuration file parsing at runtime. Optionally sets alternative config file path.
 *
 * Params:
 *     altConfigFilePath:   path to alternate config file
 *
 * Return:
 *     void
 */
void snoopski_configuration_preinit_enableAltConfigFileParsing (char * const altConfigFilePath) {
    if (NULL != altConfigFilePath) {
        snoopski_configuration_altConfigFilePath = altConfigFilePath;
    }
    snoopski_configuration_configFileParsingEnabled = SNOOPSKI_TRUE;
}


/*
 * snoopski_configuration_preinit_setConfigFilePathFromEnv
 *
 * Description:
 *     Parses environment for SNOOPSKI_INI and if found, checks if
 *     file exists and is readable, and sets path to snoopski.ini
 *     accordingly. Also it enables runtime config file parsing.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_configuration_preinit_setConfigFilePathFromEnv() {
    const char *valuePtr;


    /* Does environmental variable exist? */
    valuePtr = getenv("SNOOPSKI_INI");
    if (NULL == valuePtr) {
        /* Nope. */
        return;
    }

    /* Store it */
    strncpy(snoopski_configuration_altConfigFilePathBuf, valuePtr, PATH_MAX-1);
    snoopski_configuration_altConfigFilePathBuf[PATH_MAX-1] = 0;

    /* Is file readable? */
    if (0 != access(valuePtr, R_OK)) {
        /* Nope. */
        snoopski_configuration_altConfigFilePathBuf[0] = 0;
        return;
    }

    snoopski_configuration_preinit_enableAltConfigFileParsing(snoopski_configuration_altConfigFilePathBuf);
}


/*
 * snoopski_configuration_ctor
 *
 * Description:
 *     Populates snoopski_configuration config variable storage with
 *     correct values, either from configuration file (if enabled)
 *     or from ./configure arguments, or defaults are used as last
 *     case scenario.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_configuration_ctor() {
#ifdef SNOOPSKI_CONFIGFILE_ENABLED
    /* Is config file parsing disabled at runtime? */
    if (SNOOPSKI_FALSE == snoopski_configuration_configFileParsingEnabled) {
        return;
    }

    /* Get config pointer */
    snoopski_configuration_t *CFG = snoopski_configuration_get();

    /* Parse INI file if enabled */
    if (NULL != snoopski_configuration_altConfigFilePath) {
        // This is used by snoopski testing suite - combined tests
        snoopski_configfile_load(snoopski_configuration_altConfigFilePath);
    } else {
        snoopski_configfile_load(CFG->configfile_path);
    }
#endif
}


/*
 * snoopski_configuration_dtor
 *
 * Description:
 *     Frees all configuration-related malloced resources, and sets
 *     corresponding config settings back to their default values.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_configuration_dtor() {
    snoopski_configuration_t *CFG;

    /* Get config pointer */
    CFG = snoopski_configuration_get();

    /*
     * Reset config setting: configfile_path
     *
     * This might get changed by libsnoopski-test.so library.
     */
#ifdef SNOOPSKI_CONFIGFILE_ENABLED
    CFG->configfile_path = SNOOPSKI_CONFIGFILE_PATH;
#endif

    /*
     * Reset config setting: message_format
     */
    if (SNOOPSKI_TRUE == CFG->message_format_malloced) {
        free(CFG->message_format);

        /*
         * Set this to false - REQUIRED
         *
         * This needs to be done as a special condition can occur at boot/shutdown:
         * - snoopski is loaded when snoopski.ini is visible (mounted, present)
         * - snoopski parses it, and sets message_format and ..._malloced to TRUE
         * - on shutdown, snoopski.ini might disappear
         * - snoopski_configuration_ctor() tries to parse config file, and as it is
         *     not found, it does no alteration of snoopski_configuraton struct
         * - CFG->message_format_malloced is left set to TRUE
         * - when snoopski_configuration_dtor() is called, it tries to free the
         *     const char[] that contains the compiled-in message format
         */
        CFG->message_format_malloced = SNOOPSKI_FALSE;

        /*
         * Set this to default value - REQUIRED
         *
         * Otherwise on next snoopski run there will be no message format defined,
         * which would in best-case scenario cause no snoopski output, but in
         * worst-case scenarion there would be a segfault and possible system
         * crash.
         */
        CFG->message_format = SNOOPSKI_MESSAGE_FORMAT;
    }

    /*
     * Reset config setting: filter_chain
     */
    if (SNOOPSKI_TRUE == CFG->filter_chain_malloced) {
        free(CFG->filter_chain);

        /* Set this to false - REQUIRED (see above) */
        CFG->filter_chain_malloced = SNOOPSKI_FALSE;

        /* Set this to default value - REQUIRED (see above) */
        CFG->filter_chain = SNOOPSKI_FILTER_CHAIN;
    }

    /*
     * Reset config setting: output
     */
    if (SNOOPSKI_TRUE == CFG->output_malloced) {
        free(CFG->output);

        /* Set this to false - REQUIRED (see above) */
        CFG->output_malloced = SNOOPSKI_FALSE;

        /* Set this to default value - REQUIRED (see above) */
        CFG->output = SNOOPSKI_OUTPUT_DEFAULT;
    }

    /*
     * Reset config setting: output_arg
     */
    if (SNOOPSKI_TRUE == CFG->output_arg_malloced) {
        free(CFG->output_arg);

        /* Set this to false - REQUIRED (see above) */
        CFG->output_arg_malloced = SNOOPSKI_FALSE;

        /* Set this to default value - REQUIRED (see above) */
        CFG->output_arg = SNOOPSKI_OUTPUT_DEFAULT_ARG;
    }

    /*
     * Reset config setting: syslog_ident
     */
    if (SNOOPSKI_TRUE == CFG->syslog_ident_malloced) {
        free(CFG->syslog_ident);
        CFG->syslog_ident_malloced = SNOOPSKI_FALSE;          /* Set this to false         - REQUIRED (see above) */
        CFG->syslog_ident          = SNOOPSKI_SYSLOG_IDENT;   /* Set this to default value - REQUIRED (see above) */
    }
}


/*
 * snoopski_configuration_setUninitialized
 *
 * Description:
 *     Sets state of configuration array to uninitialized.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_configuration_setUninitialized
(
    snoopski_configuration_t *CFG
) {
    CFG->initialized = SNOOPSKI_FALSE;
}



/*
 * snoopski_configuration_setDefaults
 *
 * Description:
 *     Sets the default values for all configuration variables.
 *     Defaults are primarily defined by snoopski, and possibly overridden by
 *     ./configure flags.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_configuration_setDefaults
(
    snoopski_configuration_t *CFG
) {
    CFG->initialized             = SNOOPSKI_TRUE;

#ifdef SNOOPSKI_CONFIGFILE_ENABLED
    CFG->configfile_enabled      = SNOOPSKI_TRUE;
    CFG->configfile_path         = SNOOPSKI_CONFIGFILE_PATH;
#else
    CFG->configfile_enabled      = SNOOPSKI_FALSE;
    CFG->configfile_path         = "";
#endif
    CFG->configfile_found        = SNOOPSKI_FALSE;
    CFG->configfile_parsed       = SNOOPSKI_FALSE;

#ifdef SNOOPSKI_ERROR_LOGGING_ENABLED
    CFG->error_logging_enabled   = SNOOPSKI_TRUE;
#else
    CFG->error_logging_enabled   = SNOOPSKI_FALSE;
#endif

    CFG->message_format          = SNOOPSKI_MESSAGE_FORMAT;
    CFG->message_format_malloced = SNOOPSKI_FALSE;

    CFG->filtering_enabled       = SNOOPSKI_TRUE;
    CFG->filter_chain            = SNOOPSKI_FILTER_CHAIN;

    CFG->filter_chain_malloced   = SNOOPSKI_FALSE;

    CFG->output                  = SNOOPSKI_OUTPUT_DEFAULT;
    CFG->output_malloced         = SNOOPSKI_FALSE;
    CFG->output_arg              = SNOOPSKI_OUTPUT_DEFAULT_ARG;
    CFG->output_arg_malloced     = SNOOPSKI_FALSE;

    CFG->syslog_facility         = SNOOPSKI_SYSLOG_FACILITY;
    CFG->syslog_ident            = SNOOPSKI_SYSLOG_IDENT;
    CFG->syslog_ident_malloced   = SNOOPSKI_FALSE;
    CFG->syslog_level            = SNOOPSKI_SYSLOG_LEVEL;
}



/*
 * snoopski_configuration_get()
 *
 * Description:
 *     Retrieve pointer configuration struct.
 *
 * Params:
 *     envp:   environment array pointer to store
 *
 * Return:
 *     snoopski_configuration_t*
 */
snoopski_configuration_t* snoopski_configuration_get ()
{
    snoopski_configuration_t *CFG;

#ifdef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
    CFG = snoopski_tsrm_get_configuration();
#else
    CFG = &snoopski_configuration_data;
#endif

    if (SNOOPSKI_TRUE != CFG->initialized) {
        snoopski_configuration_setDefaults(CFG);
    }

    return CFG;
}
