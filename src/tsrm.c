/* File: tsrm.c
 *
 * Copyright (c) 2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Includes order: from local to global
 */
#include "snoopski.h"
#include "tsrm.h"

#include "configuration.h"
#include "inputdatastorage.h"

#include "lib/liblcthw/src/list.h"

#include <pthread.h>

//  Global variables
pthread_once_t       snoopski_tsrm_init_onceControl = PTHREAD_ONCE_INIT;
pthread_mutex_t      snoopski_tsrm_threadRepo_mutex;
pthread_mutexattr_t  snoopski_tsrm_threadRepo_mutexAttr;
List                 snoopski_tsrm_threadRepo_data = {
    .first = NULL,
    .last  = NULL,
    .count = 0,
};
List                *snoopski_tsrm_threadRepo = &snoopski_tsrm_threadRepo_data;


//  Non-exported function prototypes
void                        snoopski_tsrm_init                      ();
int                         snoopski_tsrm_doesThreadRepoEntryExist  (snoopski_tsrm_threadId_t threadId, int mutex_already_locked);
snoopski_tsrm_threadId_t      snoopski_tsrm_getCurrentThreadId        ();
ListNode*                   snoopski_tsrm_getCurrentThreadRepoEntry ();
snoopski_tsrm_threadData_t*   snoopski_tsrm_getCurrentThreadData      ();
snoopski_tsrm_threadData_t*   snoopski_tsrm_createNewThreadData       (snoopski_tsrm_threadId_t threadId);


/*
 * snoopski_tsrm_ctor
 *
 * Description:
 *     Initialize threadRepo list.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_tsrm_ctor() {
    snoopski_tsrm_threadId_t      curTid;
    snoopski_tsrm_threadData_t   *tData;

    // Initialize threading support
    pthread_once(&snoopski_tsrm_init_onceControl, &snoopski_tsrm_init);

    // Get my thread id - before mutex, no need for mutex here
    curTid = snoopski_tsrm_getCurrentThreadId();

    // Mutex START
    pthread_mutex_lock(&snoopski_tsrm_threadRepo_mutex);

    // Create my entry if it does not exist
    if (SNOOPSKI_FALSE == snoopski_tsrm_doesThreadRepoEntryExist(curTid, SNOOPSKI_TRUE)) {
        tData = snoopski_tsrm_createNewThreadData(curTid);
        List_push(snoopski_tsrm_threadRepo, tData);
    }

    // Mutex END
    pthread_mutex_unlock(&snoopski_tsrm_threadRepo_mutex);
}


/*
 * snoopski_tsrm_dtor
 *
 * Description:
 *     Removes own entry from threadRepo, or removes threadRepo altogether.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_tsrm_dtor() {
    ListNode                   *tRepoEntry;
    snoopski_tsrm_threadData_t   *tData;

    // Get my thread data structure
    tRepoEntry = snoopski_tsrm_getCurrentThreadRepoEntry();
    if (NULL == tRepoEntry) {
        // This is an error condition, but let's not free NULLs below
        return;
    }

    // Mutex START
    pthread_mutex_lock(&snoopski_tsrm_threadRepo_mutex);

    // Remove from repo
    tData = List_remove(snoopski_tsrm_threadRepo, tRepoEntry);

    // Mutex END
    pthread_mutex_unlock(&snoopski_tsrm_threadRepo_mutex);

    // Free the results AFTER my node has been removed from repo
    free(tData->inputdatastorage);
    free(tData->configuration);
    free(tData);

    return;
}


/*
 * snoopski_tsrm_init
 *
 * Description:
 *     Initialize threading subsystem. This function should be run
 *     only once, by only one thread.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_tsrm_init() {
    // Initialize threadRepo mutex
    pthread_mutexattr_init   (&snoopski_tsrm_threadRepo_mutexAttr);
    pthread_mutexattr_settype(&snoopski_tsrm_threadRepo_mutexAttr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init       (&snoopski_tsrm_threadRepo_mutex, &snoopski_tsrm_threadRepo_mutexAttr);
}


/*
 * snoopski_tsrm_doesThreadRepoEntryExist
 *
 * Description:
 *     Check if thread with id threadId already has an entry in snoopski's thread
 *     repository.
 *
 * Params:
 *     snoopski_tsrm_threadId_t:   Thread ID to look for
 *
 * Return:
 *     int:   SNOOPSKI_TRUE if yes, SNOOPSKI_FALSE if not
 */
int snoopski_tsrm_doesThreadRepoEntryExist (snoopski_tsrm_threadId_t threadId, int mutex_already_locked) {
    const snoopski_tsrm_threadData_t   *tData;
    int                         retVal = SNOOPSKI_FALSE;

    // Mutex START
    if (SNOOPSKI_TRUE != mutex_already_locked) {
        pthread_mutex_lock(&snoopski_tsrm_threadRepo_mutex);
    }

    LIST_FOREACH(snoopski_tsrm_threadRepo, first, next, cur) {
        if (NULL == cur->value) {
            continue;
        }
        tData = cur->value;
        if (0 != pthread_equal(threadId, tData->threadId)) {

            /* Thread ID matches */
            retVal = SNOOPSKI_TRUE;
            goto FOUND;
        }
    }

FOUND:
    // Mutex END
    if (SNOOPSKI_TRUE != mutex_already_locked) {
        pthread_mutex_unlock(&snoopski_tsrm_threadRepo_mutex);
    }

    return retVal;
}


/*
 * snoopski_tsrm_createNewThreadData
 *
 * Description:
 *     Mallocs space and creates new thread data structure, for given thread ID.
 *
 * Params:
 *     snoopski_tsrm_threadId_t:   Thread ID to create entry for
 *
 * Return:
 *     snoopski_tsrm_threadData_t*:   Pointer to newly-created threadData structure
 */
snoopski_tsrm_threadData_t*   snoopski_tsrm_createNewThreadData (snoopski_tsrm_threadId_t threadId) {
    snoopski_tsrm_threadData_t    *tData;

    // Allocte storage memory for new threadData structure
    tData                   = malloc(sizeof(snoopski_tsrm_threadData_t));
    tData->configuration    = malloc(sizeof(snoopski_configuration_t));
    tData->inputdatastorage = malloc(sizeof(snoopski_inputdatastorage_t));

    // Store thread ID
    tData->threadId         = threadId;

    // Initialize empty values
    snoopski_configuration_setUninitialized   (tData->configuration);
    snoopski_inputdatastorage_setUninitialized(tData->inputdatastorage);

    return tData;
}


/*
 * snoopski_tsrm_getCurrentThreadId
 *
 * Description:
 *     Retrieve id of current thread
 *
 * Params:
 *     (none)
 *
 * Return:
 *     snoopski_tsrm_threadId_t:   Current thread id
 */
snoopski_tsrm_threadId_t   snoopski_tsrm_getCurrentThreadId() {
    return pthread_self();
}


/*
 * snoopski_tsrm_getCurrentThreadRepoEntry
 *
 * Description:
 *     Retrieve pointer to threadRepo list node structure of current thread.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     ListNode*:   Current threadRepo entry, or null if it does not exist
 */
ListNode*   snoopski_tsrm_getCurrentThreadRepoEntry() {
    snoopski_tsrm_threadId_t      myThreadId;
    ListNode                   *tRepoEntry = NULL;
    const snoopski_tsrm_threadData_t   *tData;

    // This is the thread ID we are looking for
    myThreadId = snoopski_tsrm_getCurrentThreadId();

    // Mutex START
    pthread_mutex_lock(&snoopski_tsrm_threadRepo_mutex);

    LIST_FOREACH(snoopski_tsrm_threadRepo, first, next, cur) {

        // This should not happen, but maybe, just maybe, there is another thread
        // that is just creating new entry.
        if (NULL == cur->value) {
            continue;
        }
        tData = cur->value;

        if (0 != pthread_equal(myThreadId, tData->threadId)) {
            tRepoEntry = cur;
            goto FOUND;
        }
    }

FOUND:
    // Mutex END
    pthread_mutex_unlock(&snoopski_tsrm_threadRepo_mutex);

    // Return
    return tRepoEntry;
}


/*
 * snoopski_tsrm_getCurrentThreadData
 *
 * Description:
 *     Retrieve pointer to thread data structure of current thread. Creates
 *     a new repo entry if it does not exist.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     snoopski_tsrm_threadData_t:   Current threadRepo list entry, or NULL
 */
snoopski_tsrm_threadData_t*   snoopski_tsrm_getCurrentThreadData() {
    ListNode   *tRepoEntry;

    tRepoEntry = snoopski_tsrm_getCurrentThreadRepoEntry();
    if (NULL == tRepoEntry) {
        return NULL;
    }

    return tRepoEntry->value;
}


/*
 * snoopski_tsrm_get_configuration()
 *
 * Description:
 *     Retrieve configuration array pointer for current thread.
 *     This function is to be consumed by the rest of snoopski.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     snoopski_configuration_t*:   Pointer to thread-specific snoopski configuration data structure
 */
snoopski_configuration_t*   snoopski_tsrm_get_configuration() {
    snoopski_tsrm_threadData_t   *tData;
    tData = snoopski_tsrm_getCurrentThreadData();
    return tData->configuration;
}


/*
 * snoopski_tsrm_get_inputdatastorage()
 *
 * Description:
 *     Retrieve input data storage array pointer for current thread.
 *     This function is to be consumed by the rest of snoopski.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     snoopski_inputdatastorage_t*:   Pointer to thread-specific input data storage structure
 */
snoopski_inputdatastorage_t*   snoopski_tsrm_get_inputdatastorage() {
    snoopski_tsrm_threadData_t   *tData;
    tData = snoopski_tsrm_getCurrentThreadData();
    return tData->inputdatastorage;
}


/*
 * snoopski_tsrm_get_threadCount()
 *
 * Description:
 *     Retrieves number of currently configured threads.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     snoopski_inputdatastorage_t*:   Pointer to thread-specific input data storage structure
 */
int   snoopski_tsrm_get_threadCount() {
    int   threadCount;

    // Mutex START
    pthread_mutex_lock(&snoopski_tsrm_threadRepo_mutex);

    // Get count
    threadCount = snoopski_tsrm_threadRepo->count;

    // Mutex END
    pthread_mutex_unlock(&snoopski_tsrm_threadRepo_mutex);

    return threadCount;
}
