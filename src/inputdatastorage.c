/* File: inputdatastorage.c
 *
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Includes order: from local to global
 */
#include "inputdatastorage.h"

#include "snoopski.h"
#ifdef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
#include "tsrm.h"
#endif

/*
 * Storage of snoopski's input data for non-thread-safe builds
 */
#ifndef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
snoopski_inputdatastorage_t   snoopski_inputdatastorage_data = {
    .initialized = SNOOPSKI_FALSE,
};
#endif


/*
 * snoopski_inputdatastorage_ctor
 *
 * Description:
 *     Populates snoopski_inputdatastorage struct with default empty
 *     data, in order to prevent data leaks between execv(e) calls.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_inputdatastorage_ctor () {
    /* Get IDS pointer */
    snoopski_inputdatastorage_t *IDS = snoopski_inputdatastorage_get();

    snoopski_inputdatastorage_setDefaults(IDS);
}


/*
 * snoopski_inputdatastorage_dtor
 *
 * Description:
 *     Populates snoopski_inputdatastorage struct with default empty
 *     data, in order to prevent data leaks between execv(e) calls.
 *
 *     NOTE: This is intentional behaviour. Reset in ctor and in
 *           dtor too.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_inputdatastorage_dtor () {
    /* Get IDS pointer */
    snoopski_inputdatastorage_t *IDS = snoopski_inputdatastorage_get();

    snoopski_inputdatastorage_setDefaults(IDS);
}


/*
 * snoopski_inputdatastorage_setUninitialized()
 *
 * Description:
 *     Sets the state of IDS to uninitialized.
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_inputdatastorage_setUninitialized( snoopski_inputdatastorage_t *IDS ) {
    IDS->initialized = SNOOPSKI_FALSE;
}


/*
 * snoopski_inputdatastorage_setDefaults()
 *
 * Description:
 *     Resets the input data storage to default values
 *
 * Params:
 *     (none)
 *
 * Return:
 *     void
 */
void snoopski_inputdatastorage_setDefaults( snoopski_inputdatastorage_t *IDS ) {
    static const char * empty_string = "";
    static char * empty_string_array[] = { NULL };

    IDS->initialized = SNOOPSKI_TRUE;
    IDS->filename    = empty_string;
    IDS->argv        = empty_string_array;
    IDS->envp        = empty_string_array;
}


/*
 * snoopski_inputdatastorage_store_filename()
 *
 * Description:
 *     Store filename of execv()/execve() syscall
 *
 * Params:
 *     filename:   filename to store
 *
 * Return:
 *     void
 */
void snoopski_inputdatastorage_store_filename( const char *filename ) {
    /* Get IDS pointer */
    snoopski_inputdatastorage_t *IDS = snoopski_inputdatastorage_get();

    /* Store value */
    IDS->filename = filename;
}


/*
 * snoopski_inputdatastorage_store_argv()
 *
 * Description:
 *     Store argv[] of execv()/execve() syscall
 *
 * Params:
 *     argv:   argv pointer to store
 *
 * Return:
 *     void
 */
void snoopski_inputdatastorage_store_argv( char *const argv[] ) {
    /* Get IDS pointer */
    snoopski_inputdatastorage_t *IDS = snoopski_inputdatastorage_get();

    /* Store value */
    IDS->argv = argv;
}


/*
 * snoopski_inputdatastorage_store_envp()
 *
 * Description:
 *     Store envp[] of execve() syscall
 *
 * Params:
 *     envp:   environment array pointer to store
 *
 * Return:
 *     void
 */
void snoopski_inputdatastorage_store_envp ( char *const envp[] ) {
    /* Get IDS pointer */
    snoopski_inputdatastorage_t *IDS = snoopski_inputdatastorage_get();

    /* Store value */
    IDS->envp = envp;
}


/*
 * snoopski_inputdatastorage_get()
 *
 * Description:
 *     Retrieve inpudatastorage struct pointer
 *
 * Params:
 *     (none)
 *
 * Return:
 *     snoopski_inputdatastorage_t*:   Pointer to inputdatastorage struct
 */
snoopski_inputdatastorage_t* snoopski_inputdatastorage_get() {
    snoopski_inputdatastorage_t *IDS;

#ifdef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
    IDS = snoopski_tsrm_get_inputdatastorage();
#else
    IDS = &snoopski_inputdatastorage_data;
#endif

    if (SNOOPSKI_TRUE != IDS->initialized) {
        snoopski_inputdatastorage_setDefaults(IDS);
    }

    return IDS;
}
