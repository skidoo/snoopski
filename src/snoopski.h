/* snoopski.h
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/**
 * Include all required system headers
 */

/* This should generally be done wherever unistd.h is required. */
/* But sysconf is needed here, and all files include snoopski.h. */
/* Needed to get getpgid and getsid on older glibc */
/* This must be the first file to be included, or implicit inclusion
 * (by i.e. <features.h>) does the wrong thing
 */
#define  _XOPEN_SOURCE   700
#include <features.h>   /* Needed for GLIBC macros here */
#include <syslog.h>     /* Needed for syslog defaults */
#include <unistd.h>


/**
 * Include ./configured constants
 *
 * At least <syslog.h> must be included before including this file, as
 * LOG_... constants are needed.
 */
#include "config.h"


/**
 * SNOOPSKI_DATASOURCE_ARG_MAX_SIZE
 *
 * Maximum length of a string argument to each data source
 */
#define SNOOPSKI_DATASOURCE_ARG_MAX_SIZE 1024


/**
 * SNOOPSKI_DATASOURCE_MESSAGE_MAX_SIZE
 *
 * Maximum length of a string returned from any data source function,
 * including terminating null character.
 */
#define SNOOPSKI_DATASOURCE_MESSAGE_MAX_SIZE 2048


/**
 * SNOOPSKI_DATASOURCE_(SUCCESS|FAILURE|...)
 *
 * Datasource results
 */
#define   SNOOPSKI_DATASOURCE_SUCCESS              1
#define   SNOOPSKI_DATASOURCE_FAILURE             -1
#define   SNOOPSKI_DATASOURCE_SUCCEEDED(retVal)   (retVal >= 0)
#define   SNOOPSKI_DATASOURCE_FAILED(retVal)      (retVal <  0)


/**
 * SNOOPSKI_FILTER_CHAIN_MAX_SIZE
 *
 * Maximum length of filter chain definition
 */
#define SNOOPSKI_FILTER_CHAIN_MAX_SIZE 4096


/**
 * SNOOPSKI_FILTER_NAME_MAX_SIZE
 *
 * Maximum length of each filter name
 */
#define SNOOPSKI_FILTER_NAME_MAX_SIZE 1024


/**
 * SNOOPSKI_FILTER_ARG_MAX_SIZE
 *
 * Maximum length of a string argument to each filter
 */
#define SNOOPSKI_FILTER_ARG_MAX_SIZE 1024


/**
 * SNOOPSKI_LOG_MESSAGE_MAX_SIZE
 *
 * Maximum length of single (whole) log message,
 * including terminating null character.
 */
#define SNOOPSKI_LOG_MESSAGE_MAX_SIZE 16383


/**
 * SNOOPSKI_LOG_MESSAGE_FORMAT_default
 *
 * Default format of snoopski log message
 */
#define   SNOOPSKI_VERSION   PACKAGE_VERSION


/**
 * SNOOPSKI_MESSAGE_FORMAT
 *
 * Actual message format to use, unless configured otherwise in config file.
 * For format specification consult comments in etc/snoopski.ini.
 */
#define   SNOOPSKI_MESSAGE_FORMAT   SNOOPSKI_CONF_MESSAGE_FORMAT


/**
 * SNOOPSKI_TRUE
 * SNOOPSKI_FALSE
 */
#define   SNOOPSKI_TRUE    1
#define   SNOOPSKI_FALSE   0


/**
 * SNOOPSKI_LOG_ERROR
 * SNOOPSKI_LOG_MESSAGE
 */
#define   SNOOPSKI_LOG_ERROR     1
#define   SNOOPSKI_LOG_MESSAGE   2


/**
 * Filter return values
 *
 * SNOOPSKI_FILTER_PASS - message should be passed on
 * SNOOPSKI_FILTER_DROP - message should be dropped
 */
#define   SNOOPSKI_FILTER_PASS   1
#define   SNOOPSKI_FILTER_DROP   0


/**
 * SNOOPSKI_FILTER_CHAIN
 *
 * Actual filter chain specification to use
 *
 * Filter chain format:
 * - example: filter1; filter2; filter3:arg1; filter3:arg2
 * - you may pass argument to filter by suffixing it with :arg
 * - spaces are ignored
 */
#define   SNOOPSKI_FILTER_CHAIN   SNOOPSKI_CONF_FILTER_CHAIN


/**
 * SNOOPSKI_CONFIGFILE_*
 *
 * Path to INI configuration file, if enabled
 *
 * If configuration file is enabled, this constant is defined
 * and holds absolute path to it
 */
#ifdef SNOOPSKI_CONF_CONFIGFILE_ENABLED
#define   SNOOPSKI_CONFIGFILE_ENABLED   1
#define   SNOOPSKI_CONFIGFILE_PATH      SNOOPSKI_CONF_CONFIGFILE_PATH
#endif


/**
 * SNOOPSKI_OUTPUT
 *
 * Where is the outlet of snoopski messages
 *
 * By default, messages get sent to syslog. Groundwork for other outputs
 * is provided to facilitate unforseen uses.
 */
#ifdef SNOOPSKI_CONF_OUTPUT_DEFAULT

#define   SNOOPSKI_OUTPUT_DEFAULT          SNOOPSKI_CONF_OUTPUT_DEFAULT
#define   SNOOPSKI_OUTPUT_DEFAULT_ARG      SNOOPSKI_CONF_OUTPUT_DEFAULT_ARG

#else   /* SNOOPSKI_CONF_OUTPUT_DEFAULT */

#if (defined(__GLIBC__) && (2 == __GLIBC__) && (__GLIBC_MINOR__ < 9))
/* Use 'syslog' on older linuxes that od not support SOCK_CLOEXEC and SOCK_NONBLOCK */
#define   SNOOPSKI_OUTPUT_DEFAULT          "syslog"
#else
/* Otherwise do not use 'syslog' (was default before), because systemd is funny (blocks the syslog() call */
#define   SNOOPSKI_OUTPUT_DEFAULT          "devlog"
#endif
#define   SNOOPSKI_OUTPUT_DEFAULT_ARG      ""

#endif   /* SNOOPSKI_CONF_OUTPUT_DEFAULT */


/**
 * Output return values/macro
 *
 * SNOOPSKI_OUTPUT_SUCCESS - message was sent successfully
 * SNOOPSKI_OUTPUT_FAILURE - message was NOT sent
 */
#define   SNOOPSKI_OUTPUT_SUCCESS              1
#define   SNOOPSKI_OUTPUT_GRACEFUL_DISCARD     0
#define   SNOOPSKI_OUTPUT_FAILURE             -1
#define   SNOOPSKI_OUTPUT_SUCCEEDED(retVal)   (retVal >= 0)
#define   SNOOPSKI_OUTPUT_FAILED(retVal)      (retVal <  0)


/**
 * SNOOPSKI_LOG_ERRORS
 *
 * Whether errors are logged or not?
 *
 * If error logging is enabled, then all errors that occur when using snoopski
 * will be sent to syslog. This is to aid developers and integrators with
 * their endeavours.
 */
#ifdef SNOOPSKI_CONF_ERROR_LOGGING_ENABLED
#define   SNOOPSKI_ERROR_LOGGING_ENABLED   SNOOPSKI_CONF_ERROR_LOGGING_ENABLED
#endif


/**
 * SNOOPSKI_SYSLOG_*
 *
 * Default syslog configuration settings
 */
#define   SNOOPSKI_SYSLOG_FACILITY   SNOOPSKI_CONF_SYSLOG_FACILITY
#define   SNOOPSKI_SYSLOG_IDENT      SNOOPSKI_CONF_SYSLOG_IDENT
#define   SNOOPSKI_SYSLOG_LEVEL      SNOOPSKI_CONF_SYSLOG_LEVEL
