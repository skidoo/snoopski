/* File: datasourceregistry.c
 *
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Includes order: from local to global
 */
#include "datasourceregistry.h"

#include "snoopski.h"
#include "genericregistry.h"

#include <string.h>

/*
 * Include headers of all datasource functions
 */
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_cmdline
#include "datasource/cmdline.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_cwd
#include "datasource/cwd.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_datetime
#include "datasource/datetime.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_domain
#include "datasource/domain.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_egid
#include "datasource/egid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_egroup
#include "datasource/egroup.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_env
#include "datasource/env.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_env_all
#include "datasource/env_all.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_euid
#include "datasource/euid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_eusername
#include "datasource/eusername.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_filename
#include "datasource/filename.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_gid
#include "datasource/gid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_group
#include "datasource/group.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_hostname
#include "datasource/hostname.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_login
#include "datasource/login.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_pid
#include "datasource/pid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_ppid
#include "datasource/ppid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_rpname
#include "datasource/rpname.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_sid
#include "datasource/sid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_configure_command
#include "datasource/snoopski_configure_command.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_literal
#include "datasource/snoopski_literal.h"
#endif
#ifdef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_threads
#include "datasource/snoopski_threads.h"
#endif
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_version
#include "datasource/snoopski_version.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tid
#include "datasource/tid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tid_kernel
#include "datasource/tid_kernel.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp
#include "datasource/timestamp.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp_ms
#include "datasource/timestamp_ms.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp_us
#include "datasource/timestamp_us.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty
#include "datasource/tty.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty_uid
#include "datasource/tty_uid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty_username
#include "datasource/tty_username.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_uid
#include "datasource/uid.h"
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_username
#include "datasource/username.h"
#endif

/* This prevents "ISO C forbids empty initializer braces" error */
#include "datasource/noop.h"



/*
 * Two arrays holding data about all data sources
 */
char* snoopski_datasourceregistry_names[] = {
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_cmdline
    "cmdline",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_cwd
    "cwd",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_datetime
    "datetime",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_domain
    "domain",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_egid
    "egid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_egroup
    "egroup",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_env
    "env",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_env_all
    "env_all",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_euid
    "euid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_eusername
    "eusername",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_filename
    "filename",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_gid
    "gid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_group
    "group",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_hostname
    "hostname",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_login
    "login",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_pid
    "pid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_ppid
    "ppid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_rpname
    "rpname",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_sid
    "sid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_configure_command
    "snoopski_configure_command",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_literal
    "snoopski_literal",
#endif
#ifdef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_threads
    "snoopski_threads",
#endif
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_version
    "snoopski_version",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tid
    "tid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tid_kernel
    "tid_kernel",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp
    "timestamp",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp_ms
    "timestamp_ms",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp_us
    "timestamp_us",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty
    "tty",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty_uid
    "tty_uid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty_username
    "tty_username",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_uid
    "uid",
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_username
    "username",
#endif

    /* This prevents "ISO C forbids empty initializer braces" error */
    "noop",
    "",
};

int (*snoopski_datasourceregistry_ptrs []) (char * const result, char const * const arg) = {
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_cmdline
    snoopski_datasource_cmdline,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_cwd
    snoopski_datasource_cwd,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_datetime
    snoopski_datasource_datetime,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_domain
    snoopski_datasource_domain,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_egid
    snoopski_datasource_egid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_egroup
    snoopski_datasource_egroup,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_env
    snoopski_datasource_env,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_env_all
    snoopski_datasource_env_all,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_euid
    snoopski_datasource_euid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_eusername
    snoopski_datasource_eusername,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_filename
    snoopski_datasource_filename,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_gid
    snoopski_datasource_gid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_group
    snoopski_datasource_group,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_hostname
    snoopski_datasource_hostname,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_login
    snoopski_datasource_login,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_pid
    snoopski_datasource_pid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_ppid
    snoopski_datasource_ppid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_rpname
    snoopski_datasource_rpname,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_sid
    snoopski_datasource_sid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_configure_command
    snoopski_datasource_snoopski_configure_command,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_literal
    snoopski_datasource_snoopski_literal,
#endif
#ifdef SNOOPSKI_CONF_THREAD_SAFETY_ENABLED
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_threads
    snoopski_datasource_snoopski_threads,
#endif
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_snoopski_version
    snoopski_datasource_snoopski_version,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tid
    snoopski_datasource_tid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tid_kernel
    snoopski_datasource_tid_kernel,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp
    snoopski_datasource_timestamp,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp_ms
    snoopski_datasource_timestamp_ms,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_timestamp_us
    snoopski_datasource_timestamp_us,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty
    snoopski_datasource_tty,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty_uid
    snoopski_datasource_tty_uid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_tty_username
    snoopski_datasource_tty_username,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_uid
    snoopski_datasource_uid,
#endif
#ifdef SNOOPSKI_CONF_DATASOURCE_ENABLED_username
    snoopski_datasource_username,
#endif

    /* This prevents "ISO C forbids empty initializer braces" error */
    snoopski_datasource_noop,
};


/*
 * getCount()
 * Return number of available datasources
 */
int snoopski_datasourceregistry_getCount () {
    return snoopski_genericregistry_getCount(snoopski_datasourceregistry_names);
}


/*
 * doesIdExist()
 * True if datasource exists (by id), otherwise false
 */
int snoopski_datasourceregistry_doesIdExist (int datasourceId) {
    return snoopski_genericregistry_doesIdExist(snoopski_datasourceregistry_names, datasourceId);
}


/*
 * doesNameExist()
 * True if datasource exists (by name), otherwise false
 */
int snoopski_datasourceregistry_doesNameExist (char const * const datasourceName) {
    return snoopski_genericregistry_doesNameExist(snoopski_datasourceregistry_names, datasourceName);
}


/*
 * getIdFromName()
 * Return index of given datasource, or -1 if not found
 */
int snoopski_datasourceregistry_getIdFromName (char const * const datasourceName) {
    return snoopski_genericregistry_getIdFromName(snoopski_datasourceregistry_names, datasourceName);
}


/*
 * getName()
 * Return name of given datasource, or NULL
 */
char* snoopski_datasourceregistry_getName (int datasourceId) {
    return snoopski_genericregistry_getName(snoopski_datasourceregistry_names, datasourceId);
}


/*
 * callById()
 * Call the given datasource by id and return its output
 */
int snoopski_datasourceregistry_callById (int datasourceId, char * const result, char const * const datasourceArg) {
    if (SNOOPSKI_FALSE == snoopski_datasourceregistry_doesIdExist(datasourceId)) {
        return -1;
    }

    return snoopski_datasourceregistry_ptrs[datasourceId](result, datasourceArg);
}


/*
 * callByName()
 * Call the given datasource by name and return its output
 */
int snoopski_datasourceregistry_callByName (char const * const datasourceName, char * const result, char const * const datasourceArg) {
    int datasourceId;

    datasourceId = snoopski_datasourceregistry_getIdFromName(datasourceName);
    if (datasourceId == -1) {
        return -1;
    }

    return snoopski_datasourceregistry_ptrs[datasourceId](result, datasourceArg);
}
