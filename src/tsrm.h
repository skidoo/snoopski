/* File: tsrm.h
 *
 * Copyright (c) 2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

//  START: Prevent cyclic inclusions
#ifndef __SNOOPSKI_TSRM_H
#define __SNOOPSKI_TSRM_H

/*
 * Includes order: from local to global
 */
#include "configuration.h"
#include "inputdatastorage.h"

#include <pthread.h>

/*
 * Type alias: threadId
 */
typedef   pthread_t   snoopski_tsrm_threadId_t;


//  Type: thread data struct
typedef struct {
    snoopski_tsrm_threadId_t     threadId;
    snoopski_configuration_t    *configuration;
    snoopski_inputdatastorage_t *inputdatastorage;
} snoopski_tsrm_threadData_t;


//  Init/shutdown functions
void   snoopski_tsrm_ctor ();
void   snoopski_tsrm_dtor ();

// Getter functions
snoopski_configuration_t*    snoopski_tsrm_get_configuration    ();
snoopski_inputdatastorage_t* snoopski_tsrm_get_inputdatastorage ();
int                        snoopski_tsrm_get_threadCount      ();


//  END: Prevent cyclic inclusion
#endif   /* Cyclic inclusion */
