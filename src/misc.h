/* File: misc.h
 *
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

//  Init/shutdown functions
void snoopski_init    ();
void snoopski_cleanup ();

//  String functions
void snoopski_string_append     (char *destString, const char *appendThis, int destStringMaxLength);
int  snoopski_string_countChars (const char *stringToSearch, char characterToCount);


//  Syslog functions
int         snoopski_syslog_convert_facilityToInt (const char *facilityStr);
const char* snoopski_syslog_convert_facilityToStr (int   facilityInt);
int         snoopski_syslog_convert_levelToInt    (const char *levelStr);
const char* snoopski_syslog_convert_levelToStr    (int   levelInt);
