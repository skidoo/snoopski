/* File: filterregistry.c
 *
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Includes order: from local to global
 */
#include "filterregistry.h"

#include "snoopski.h"
#include "genericregistry.h"

/*
 * Include headers of all filter functions
 */
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_exclude_spawns_of
#include "filter/exclude_spawns_of.h"
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_exclude_uid
#include "filter/exclude_uid.h"
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_root
#include "filter/only_root.h"
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_tty
#include "filter/only_tty.h"
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_uid
#include "filter/only_uid.h"
#endif

/* This prevents "ISO C forbids empty initializer braces" error */
#include "filter/noop.h"


/*
 * Two arrays holding data about filter functions
 */
char *snoopski_filterregistry_names[] = {
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_exclude_spawns_of
    "exclude_spawns_of",
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_exclude_uid
    "exclude_uid",
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_root
    "only_root",
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_tty
    "only_tty",
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_uid
    "only_uid",
#endif

    /* This prevents "ISO C forbids empty initializer braces" error */
    "noop",
    "",
};

int (*snoopski_filterregistry_ptrs []) (char *logMessage, char const * const arg) = {
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_exclude_spawns_of
    snoopski_filter_exclude_spawns_of,
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_exclude_uid
    snoopski_filter_exclude_uid,
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_root
    snoopski_filter_only_root,
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_tty
    snoopski_filter_only_tty,
#endif
#ifdef SNOOPSKI_CONF_FILTER_ENABLED_only_uid
    snoopski_filter_only_uid,
#endif

    /* This prevents "ISO C forbids empty initializer braces" error */
    snoopski_filter_noop,
};


/*
 * getCount()
 * Return number of available filters
 */
int snoopski_filterregistry_getCount () {
    return snoopski_genericregistry_getCount(snoopski_filterregistry_names);
}


/*
 * doesIdExist()
 * True if filter exists (by id), otherwise false
 */
int snoopski_filterregistry_doesIdExist (int filterId) {
    return snoopski_genericregistry_doesIdExist(snoopski_filterregistry_names, filterId);
}


/*
 * doesNameExist()
 * True if filter exists (by name), otherwise false
 */
int snoopski_filterregistry_doesNameExist (char const * const filterName) {
    return snoopski_genericregistry_doesNameExist(snoopski_filterregistry_names, filterName);
}


/*
 * getIdFromName()
 * Return index of given filter, or -1 if not found
 */
int snoopski_filterregistry_getIdFromName (char const * const filterName) {
    return snoopski_genericregistry_getIdFromName(snoopski_filterregistry_names, filterName);
}


/*
 * getName()
 * Return name of given filter, or NULL
 */
char* snoopski_filterregistry_getName (int filterId) {
    return snoopski_genericregistry_getName(snoopski_filterregistry_names, filterId);
}


/*
 * callById()
 * Call the given filter by id and return its output
 */
int snoopski_filterregistry_callById (int filterId, char *logMessage, char const * const filterArg) {
    if (SNOOPSKI_FALSE == snoopski_filterregistry_doesIdExist(filterId)) {
        return -1;
    }

    return snoopski_filterregistry_ptrs[filterId](logMessage, filterArg);
}


/*
 * callByName()
 * Call the given filter by name and return its output
 */
int snoopski_filterregistry_callByName (char const * const filterName, char * logMessage, char const * const filterArg) {
    int filterId;

    filterId = snoopski_filterregistry_getIdFromName(filterName);
    if (filterId == -1) {
        return -1;
    }

    return snoopski_filterregistry_ptrs[filterId](logMessage, filterArg);
}
