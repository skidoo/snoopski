## What is snoopski?

 snoopski is a tiny utility that logs all program executions on your Linux system.

 snoopski logger provides a shared library (a libc execve() wrapper), as well as
 2 utility scripts which enable/disable LD_PRELOAD injection of libsnoopski.
 Configurable choice of logged details can be specified within /etc/snoopski.ini,
 useful toward performing light/heavy system monitoring, and toward gaining a
 good sense of what's going on in the system (for example apache running many,
 and short-lived, cgi scripts).


doc/HACKING.md outlines how it actually accomplishes that (a fairly technical read).

This is what typical snoopski output looks like:

    2015-02-11T19:05:10+00:00 labrat-1 snoopski[896]: [uid:0 sid:11679 tty:/dev/pts/2 cwd:/root filename:/usr/bin/cat]: cat /etc/fstab.BAK
    2015-02-11T19:05:15+00:00 labrat-1 snoopski[896]: [uid:0 sid:11679 tty:/dev/pts/2 cwd:/root filename:/usr/bin/rm]: rm -f /etc/fstab.BAK
    2015-02-11T19:05:19+00:00 labrat-1 snoopski[896]: [uid:0 sid:11679 tty:/dev/pts/2 cwd:/root filename:/usr/bin/tail]: tail -f /var/log/messages

The _default_ output location is: `/var/log/auth.log`

For actual output format and destination, check your snoopski and syslog configuration.
(BETTER: prior to first-run, edit /etc/snoopski.ini to specify YOUR preferred
logfile location and your desired formatting for the message output.)



## Configuration

If the configuration file support is available in your snoopski build (it probably is), snoopski can be reconfigured on-the-fly.

The configuration file is (most likely, but depending on the build) located at `/etc/snoopski.ini`.

Supported configuration directives are explained in the [default configuration file](etc/snoopski.ini.in).

______________________________

## snoopski Logger FAQ

### I see no snoopski output after initial user login

Your user probably has LD_PRELOAD environmental variable set to something non-empty empty in their ~/.profile script.
Remove it, or add `libsnoopski.so` to LD_PRELOAD, like this (UNTESTED):

    export LD_PRELOAD="/path/to/libsnoopski.so /path/to/otherlib.so"

(NOTE: CURRENTLY, NON-SUDO PER-USER (vs SYSTEMWIDE) USAGE IS UNDOCUMENTED//UNTESTED)


### How do I go about developing new data source/filter/output?

Consult doc/HACKING.md (within the gitlab -hosted project repository) for more information.

### Why do I not see any non-root snoopski entries in my logfile?

If you have configured `file` output provider, make sure the target file has proper permissions set for writing into it by all users.
This usually means changing file permissions to world-writable `0666`, which is a fairly insecure setting.
You may want to reconfigure snoopski to use `devlog` output instead.

(THE ABOVE ATTEMPTS TO CLARIFY THE FACT THAT ALTHOUGH /sbin/snoopski-enable is installed 0755,
A CUSTOM-PATHED SNOOPSKI LOGFILE DOES NEED TO BE WORLD-WRITABLE.)


_____________________________

## Security disclaimer

**WARNING: snoopski is NOT a reliable auditing solution.**

Rogue users can easily manipulate environment to avoid their actions being logged by snoopski.
Consult FAQ entry "i-see-no-snoopski-output-after-initial-user-login" for more information.


## Credits

snoopski was forked from "snoopy" https://github.com/a2o/snoopy

## License

snoopski is released under [GNU General Public License version 2]
