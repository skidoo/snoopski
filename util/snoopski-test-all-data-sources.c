/*
 * File: snoopski-test-all-data-sources.c
 *
 * Copyright (c) 2014-2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snoopski.h"
#include "configuration.h"
#include "error.h"
#include "inputdatastorage.h"
#include "libsnoopski-debug-addons.h"
#include "misc.h"



/*
 * We do not use separate .h file here
 */
int  main (int argc, char **argv);
void snoopski_test_all_datasources ();
int  snoopski_get_datasource_name_length_max ();



int main (int argc, char **argv)
{
    snoopski_init();
    snoopski_inputdatastorage_store_filename(argv[0]);
    snoopski_inputdatastorage_store_argv(argv);

    snoopski_debug_test_all_datasources();

    snoopski_cleanup();
    return 0;
}
