/*
 * File: snoopski-test-filter.c
 *
 * Copyright (c) 2015 Bostjan Skufca <bostjan@a2o.si>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*
 * Includes order: from local to global
 */
//#include "snoopski-test-filter.h"

#include "snoopski.h"
#include "libsnoopski-debug-addons.h"

#include "configuration.h"
#include "filtering.h"

#include "log.h"
#include "message.h"
#include "misc.h"
#include "inputdatastorage.h"

#include <stdio.h>
#include <stdlib.h>



int main (int argc, char **argv);
int main (int argc, char **argv) {
    char                     *logMessage = NULL;
    snoopski_configuration_t   *CFG;

    snoopski_init();
    snoopski_inputdatastorage_store_filename(argv[0]);
    snoopski_inputdatastorage_store_argv(argv);

    /* Get config pointer */
    CFG = snoopski_configuration_get();

    /* Initialize empty log message */
    logMessage    = malloc(SNOOPSKI_LOG_MESSAGE_MAX_SIZE);
    logMessage[0] = '\0';


    /* Run through as much code as possible */
    printf("-----[ Datasources ]-----------------------------------\n");
    snoopski_debug_test_all_datasources();

    printf("-----[ Filters ]---------------------------------------\n");
    snoopski_debug_test_all_filters();

    printf("-----[ Outputs ]---------------------------------------\n");
    snoopski_debug_test_all_outputs();

    printf("-----[ Message formatting ]----------------------------\n");
    snoopski_message_generateFromFormat(logMessage, CFG->message_format);
    printf("Message: %s\n", logMessage);

    printf("-----[ Filtering ]-------------------------------------\n");
    snoopski_filtering_check_chain(logMessage, "exclude_uid:10,11,12;only_uid=0,1,2,3");
    printf("Done.\n");

    printf("-----[ Dispatching ]-----------------------------------\n");
    snoopski_log_dispatch(logMessage, SNOOPSKI_LOG_MESSAGE);
    printf("Done.\n");

    printf("\nAll done.\n");

    free(logMessage);
    snoopski_cleanup();

    /* Close these FDs too, otherwise valgrind complains */
    fclose(stdin);
    fclose(stdout);
    fclose(stderr);

    return 0;
}
