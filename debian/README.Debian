snoopski
=========

Log format is:

[login:%{login} ssh:(%{env:SSH_CONNECTION}) sid:%{sid} tty:%{tty} (%{tty_uid}/%{tty_username}) uid:%{username}(%{uid})/%{eusername}(%{euid}) gid:%{group}(%{gid})/%{egroup}(%{egid}) cwd:%{cwd}]: %{cmdline}

NOTE: Above is intended to illustrate all possible variables.
The current as-shipped default configuration, per debian/rules in this source package is:
         --with-message_format='%{datetime}   cmdline=%{cmdline}      %{filename}'


Explanation of the available variables:

- login: literal login name of logged-in user executing this process. Retrieves the user login, trying, in order:
     ~ The login information from the process
     ~ the LOGNAME environment variable
     ~ the SUDO_USER environment variable
     returns "unknown" otherwise.

     TIP -- to use with sudo and keep LOGNAME, add this in /etc/sudoers:
        Defaults        env_reset
        Defaults        env_keep="LOGNAME"
     (on an antiX system,  /etc/sudoers.d/antixers would be a more appropriate place to specify the above)

- ssh:          value of SSH_CONNECTION
- sid:          Session leader process ID of current process
- tty:          TTY of current process.
- tty_uid:      UID (User ID) of current controlling terminal, or -1 if not found
- tty_username: litaral username of current controlling terminal
- username:     literal username of current process/
- uid: UID      (User ID) of current process
- eusername:    literal effective user name (User ID) of current process/
- euid:         effective UID of current process
- group:        literal group name (Group ID) of current process
- gid:          GID (Group ID) number of currently running process
- egroup:       literal effective group name (Group ID) of current process
- egid:         effective UID of current process
- cwd:          current working directory of current process
- cmdline:      commandline of current process

The output to logfile is, unconditionally, one line per logged event.
Any backslash+n, or extra spaces, or any other characters specified within your custom message format template will be parsed as literals.
